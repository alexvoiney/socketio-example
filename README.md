SOCKET.io example
================

## Running server ##

    docker run --rm -p 6379:6379 socketio-example-redis -d redis
    rq worker --with-scheduler
    FLASK_APP=server FLASK_APP_SETTINGS=config.ini.example flask run


## Running client ##

    python -m http.server 8000 --bind 127.0.0.1