from flask import request
from flask_restful import Resource

from server.tasks import do_a_thing
from server import q, api


class JobList(Resource):
    def post(self):
        data = request.json['data']
        job = q.enqueue(do_a_thing, data)
        return {"status": "accepted"}


api.add_resource(JobList, '/job/')