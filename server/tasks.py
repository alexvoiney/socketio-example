from server import socketio
import time


def do_a_thing(data):
    time.sleep(3)
    socketio.emit("downloaded", {"status": "done", "results": []}, json=True)
