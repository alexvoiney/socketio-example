from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from flask_cors import CORS
from flask_restful import Api
from redis import Redis
from rq import Queue
import os

# configure app
app = Flask(__name__)
app.config.from_envvar('FLASK_APP_SETTINGS')
cors = CORS(app, resources={r"/*": {"origins": app.config.get("ALLOWED_ORIGINS")}})

# configure socketIO
socketio = SocketIO(app, cors_allowed_origins=app.config.get("ALLOWED_ORIGINS"), message_queue="redis://" + app.config.get("REDIS_URL", ""))

# configure RQ
redis_host, redis_port = app.config.get("REDIS_URL", "localhost:6379").split(":")
redis_conn = Redis(redis_host, int(redis_port))
q = Queue(connection=redis_conn)

# define API
api = Api(app)

import server.resources



if __name__ == '__main__':
    socketio.run(app, host="127.0.0.1")
